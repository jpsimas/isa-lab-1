pkg load signal;

groupNum = 12;
p = mod(groupNum,2);

x = 14;%de Omena Simas
y = 12;%kattakkayath

N = (2^p) * (mod(x, 2) + 1) + 6*p;%IIR order
nb = mod(y, 7) + 8;%number of bits

csvwrite("n.txt", N);
csvwrite("nb.txt", nb);

%filter design
cutoff = 2e3;%cuttof frequency
fs = 10e3;%sample rate
[b1, a1] = butter(N, cutoff/(fs/2));

%convert b and a coefficient vectors to form y_k = sum_{i=0}^{Na}b_i x[k-i] + sum_{i=1}^{Na}a_i x[k-i]

b = b1/a1(1);
a = -a1(2:end)/a1(1);

bi=floor(b*2^(nb-1)); %% convert b coefficients into nb-bit integers
bq=bi*2^-(nb-1); %% convert back b coefficients as nb-bit real values
ai=floor(a*2^(nb-1)); %% convert a coefficients into nb-bit integers
aq=ai*2^-(nb-1); %% convert back a coefficients as nb-bit real values

freqz(bq, [1 -aq], 1000, fs);
csvwrite("a.txt", ai.');
csvwrite("b.txt", bi.');

%coefficients of the unfolded filter

n_a = length(a);
n_b = length(b);
a_new = zeros(n_a + 1, 1);
b_new = zeros(n_b + 1, 1);

a_new(2:n_a) = a(2:n_a) + a(1)*a((2:n_a) - 1);
a_new(n_a+1) = a(1)*a(n_a);

b_new(1) = b(1);
b_new(2:n_b) = b(2:n_b) + a(1)*b((2:n_b) - 1);
b_new(n_b+1) = a(1)*b(n_b);

b_newi=floor(b_new*2^(nb-1)); %% convert b coefficients into nb-bit integers
a_newi=floor(a_new*2^(nb-1)); %% convert a coefficients into nb-bit integers

csvwrite("a_modified.txt", a_newi);
csvwrite("b_modified.txt", b_newi);
