nb = csvread("nb.txt");
fc = 2/10;
N = 300;

X(1:floor(N/4)) = 0;
X((floor(N/4)+1):(2*floor(N/4))) = 0.5*(1 - 2^-(nb-1))*sin(2*pi*(fc/10)*((floor(N/4)+1):(2*floor(N/4))));
X((2*floor(N/4)+1):(3*floor(N/4))) = 0.5*(1 - 2^-(nb-1))*sin(2*pi*fc*((2*floor(N/4)+1):(3*floor(N/4))));
X((3*floor(N/4)+1):N) = 0.5*(1 - 2^-(nb-1));

Xq = floor(X.'*2^(nb-1));
csvwrite("input.txt", Xq);

a = ([1 -(csvread("a.txt")*2^-(nb-1)).'].');
b = csvread("b.txt")*2^-(nb-1);

y = filter(b, a, Xq*2^-(nb-1));
figure;
hold on;
plot(Xq*2^-(nb-1));
plot(y);

legend("in", "out");
