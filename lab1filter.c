#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int myfilter(int n, int nb, int x, int* a, int* b) {
  static int *shift_reg_in; // x shift register
  static int *shift_reg_out; // y shift register
  static bool first_run = false; // for cleaning shift registers

  // clean the buffers
  if(!first_run) {
    first_run = true;

    shift_reg_in = (int*) malloc((n+1)*sizeof(int));
    shift_reg_out = (int*) malloc((n+1)*sizeof(int));
    
    for(int i = 0; i < n+1; i++) {
      shift_reg_in[i] = 0;
      shift_reg_out[i] = 0;
    }
  }

  // shift and insert new sample in x shift register
  for(int i = n; i > 0; i--)
    shift_reg_in[i] = shift_reg_in[i-1];
  shift_reg_in[0] = x;

  // make the convolution
  // Moving average part
  int ret = 0;
  for(int i = 0; i < n+1; i++)
    ret += (shift_reg_in[i]*b[i])/(1 << (nb-1));//right shift of int is undefined behavior
  // Auto regressive part
  for(int i = 0; i < n+1; i++)
    ret += (shift_reg_out[i]*a[i])/(1 << (nb-1));

  // update the y shift register
  for(int i = n; i > 0; i--)
    shift_reg_out[i] = shift_reg_out[i-1];
  shift_reg_out[0] = ret;
 
  return ret;
}

int main(int argc, char **argv) {
  FILE *file_in, *file_out;
  int n, nb;
  int *a, *b;

  //read parameters
  file_in = fopen("n.txt", "r");
  if(file_in == NULL) {
    printf("Error: cannot open n.txt\n");
    exit(2);
  }
  fscanf(file_in, "%d", &n);
  fclose(file_in);

  file_in = fopen("nb.txt", "r");
  if(file_in == NULL) {
    printf("Error: cannot open nb.txt\n");
    exit(2);
  }
  fscanf(file_in, "%d", &nb);
  fclose(file_in);

  //allocate coefficient vectors
  a = (int*) malloc((n+1)*sizeof(int));
  b = (int*) malloc((n+1)*sizeof(int));

  //read coefficients
  file_in = fopen("a.txt", "r");
  FILE *file_in2 = fopen("b.txt", "r");
  
  for(int i = 0; i < n+1; i++){
    fscanf(file_in, "%d", a+i);
    fscanf(file_in2, "%d", b+i);
  }
  fclose(file_in);
  fclose(file_in2);
  
  // check the command line
  if(argc != 3) {
    printf("Use: %s <input_file> <output_file>\n", argv[0]);
    exit(1);
  }

  // open files
  file_in = fopen(argv[1], "r");
  if(file_in == NULL) {
    printf("Error: cannot open %s\n", argv[1]);
    exit(2);
  }
  
  file_out = fopen(argv[2], "w");
  if(file_out == NULL) {
    printf("Error: cannot open %s\n", argv[2]);
    exit(2);
  }

  int input, output;

  // get samples and apply filter
  fscanf(file_in, "%d", &input);
  do {
    output = myfilter(n, nb, input, a, b);
    fprintf(file_out,"%d\n", output);
    fscanf(file_in, "%d", &input);
    /* printf("read: %d\n", input); */
  } while(!feof(file_in));

  fclose(file_in);
  fclose(file_out);
  
  return 0;
}
